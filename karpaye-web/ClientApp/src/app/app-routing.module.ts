import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { ArticlesComponent } from './articles/articles.component';
import { ComplaintsComponent } from './complaints/complaints.component';
import { ContactusComponent } from './contactus/contactus.component';
import { QuestionsComponent } from './questions/questions.component';
import { RegisterComponent } from './register/register.component';
const routes: Routes = [
  { path: 'homepage', component:HomepageComponent },
  { path: 'articles', component: ArticlesComponent },
  { path: 'complaints', component: ComplaintsComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'questions', component: QuestionsComponent },
  { path:'register',component:RegisterComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
