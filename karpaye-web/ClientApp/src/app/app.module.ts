import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { QuestionsComponent } from './questions/questions.component';
import { ContactusComponent } from './contactus/contactus.component';

import { MatButtonModule, MatMenuModule, MatSidenavModule, MatIconModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { ArticlesComponent } from './articles/articles.component';
import { ComplaintsComponent } from './complaints/complaints.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomepageComponent } from './homepage/homepage.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { LawComponent } from './law/law.component';
import { HomeHeaderComponent } from './homepage/home-header/home-header.component';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';
import { SamandehiComponent } from './footer/samandehi/samandehi.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    QuestionsComponent,
    ContactusComponent,
    ArticlesComponent,
    ComplaintsComponent,
    HomepageComponent,
    AboutusComponent,
    LawComponent,
    HomeHeaderComponent,
    RegisterComponent,
    SearchComponent,
    SamandehiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule, MatMenuModule, MatSidenavModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatCardModule,
    FormsModule,
    MatIconModule,
    MDBBootstrapModule.forRoot()
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
