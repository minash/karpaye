import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SamandehiComponent } from './samandehi.component';

describe('SamandehiComponent', () => {
  let component: SamandehiComponent;
  let fixture: ComponentFixture<SamandehiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SamandehiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SamandehiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
