import { Component, OnInit ,AfterViewInit, ViewChildren} from '@angular/core';
import { Observable, interval, Subscription } from 'rxjs';


@Component({
  selector: 'app-home-header',
  templateUrl: './home-header.component.html',
  styleUrls: ['./home-header.component.scss']
})
export class HomeHeaderComponent implements OnInit{
  showMessage: boolean = false;
  showSave: boolean = false;
  title = 'app works!';

  save() {
    this.showSave = !this.showSave;
    setTimeout(() => {
      this.showSave = !this.showSave;
    }, 2000)

  }
  private subscription: Subscription;
  private timer: Observable<any>;

  public ngOnInit() {


    interval(2000).subscribe(x => {
      this.showMessage = true;
      setTimeout(() => { this.showMessage = false; }, 1000);
    });

  }


}
