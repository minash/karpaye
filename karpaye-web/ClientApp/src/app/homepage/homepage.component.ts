import { Component, OnInit } from '@angular/core';
import { Typed }from 'typed.js/src/typed.js';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  constructor() {

  }

  ngOnInit() {
    var typed = new Typed(".typed", {
      strings: '.typed-text',
      smartBackspace: true // Default value
    });
  }
}
