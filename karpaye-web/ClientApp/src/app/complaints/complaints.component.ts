import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-complaints',
  templateUrl: './complaints.component.html',
  styleUrls: ['./complaints.component.scss']
})
export class ComplaintsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @ViewChild('f') form: NgForm;
 com = {
    username: '',
    email: '',
    subject:''
   
  };
  submitted = false;

  onSubmit() {
    this.submitted = true;
    this.com.username = this.form.value.userData.username;
    this.com.email = this.form.value.userData.email;
    this.form.reset();
  }
}
